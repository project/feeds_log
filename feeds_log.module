<?php

/**
 * @file
 * Module Handle the feeds unimported report log handling.
 */

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Implements HOOK_feeds_unimported_record().
 */
function feeds_log_feeds_unimported_record($entity, $feed) {
  $violations = $entity->validate();
  if (!count($violations)) {
    return;
  }

  $errors = [];

  foreach ($violations as $violation) {
    $error = $violation->getMessage();

    // Try to add more context to the message.
    // @todo if an exception occurred because of a different bundle, add more
    // context to the message.
    $invalid_value = $violation->getInvalidValue();
    if ($invalid_value instanceof FieldItemListInterface) {
      // The invalid value is a field. Get more information about this field.
      $error = new FormattableMarkup('@name : @error', [
        '@name' => $invalid_value->getFieldDefinition()->getLabel(),
        '@error' => $error,
      ]);
    }
    else {
      $error = new FormattableMarkup('@property_name: @error', [
        '@property_name' => $violation->getPropertyPath(),
        '@error' => $error,
      ]);
    }

    $errors[] = $error;
  }

  $element = [
    '#theme' => 'item_list',
    '#items' => $errors,
  ];
  $label = $entity->label();
  $guid = $entity->get('feeds_item')->guid;
  $messages = [];
  $args = [
    '%label' => $label,
    '%guid' => $guid,
    '@errors' => \Drupal::service('renderer')->render($element),
  ];
  if ($guid || $guid === '0' || $guid === 0) {
    $messages[] = t('GUID %guid : @errors <br>', $args);
  }
  else {
    $messages[] = t('@errors ', $args);
  }
  $message_element = [
    '#markup' => implode("<br>", $messages),
  ];
  $msg = strip_tags($message_element['#markup'], '<li></li><ul></ul>');
  $arrFields = [
    'fid' => (!empty($feed->id())) ? $feed->id() : '',
    'label' => (!empty($label)) ? trim($label) : '',
    'imported' => \Drupal::time()->getRequestTime(),
    'message' => (!empty($msg)) ? trim($msg) : '',
  ];
  // @todo Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
  // You will need to use `\Drupal\core\Database\Database::getConnection()` if you do not yet have access to the container here.
  \Drupal::database()->insert('feeds_log')->fields($arrFields)->execute();
}

/**
 * Implements HOOK_feeds_feed_delete().
 */
function feeds_log_feeds_feed_multiple_delete($feedsEntity) {
  if (count($feedsEntity) > 0) {
    $arrFid = [];
    foreach ($feedsEntity as $strVal) {
      $arrFid[] = $strVal->fid->value;
    }
    // @todo Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // You will need to use `\Drupal\core\Database\Database::getConnection()` if you do not yet have access to the container here.
    \Drupal::database()->delete('feeds_log')->condition('fid', $arrFid, 'IN')->execute();
  }
}

/**
 * Implements HOOK_feeds_feed_delete().
 */
function feeds_log_feeds_feed_delete($feedId) {
  if (!empty($feedId)) {
    // @todo Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // You will need to use `\Drupal\core\Database\Database::getConnection()` if you do not yet have access to the container here.
    \Drupal::database()->delete('feeds_log')->condition('fid', $feedId)->execute();
  }
}
